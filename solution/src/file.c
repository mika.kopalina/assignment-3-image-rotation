#include "transformations.h"
#include "file.h"

FILE* open_file_read(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        perror("Ошибка при открытии файла на чтение");
    }
    return file;
}

FILE* open_file_write(const char* filename) {
    FILE* file = fopen(filename, "wb");
    if (file == NULL) {
        perror("Ошибка при открытии файла на запись");
    }
    return file;
}
