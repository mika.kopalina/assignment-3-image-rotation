#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct pixel pixel_new(int8_t r, int8_t g, int8_t b) {
    return (struct pixel) {r, g, b};
}

// Создание изображения с заданными размерами
struct image image_makenew(const uint64_t width, const uint64_t height) {
    struct pixel *data = malloc(width * height * sizeof(struct pixel));
    if (data == NULL) {
        // Если не удалось выделить память для данных, освобождаем память и возвращаем пустую структуру
        return (struct image) {0};
    }
    return (struct image) {
                .width = width,
                .height = height,
                .data = data
                };
}


// Освобождение памяти, занятой изображением
void image_delete(struct image* img) {
    // Проверка, что img не равен NULL
    if (img == NULL) {
        return;
    }
    // Освобождение памяти, выделенной под пиксели, если она не равна NULL
    if (img->data != NULL) {
        free(img->data);
        img->data = NULL;
    }
    // Обнуление полей структуры
    img->width = 0;
    img->height = 0;
}








