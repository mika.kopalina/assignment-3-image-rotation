#include "validation.h"
#define X(name, value) value,
#undef X


uint64_t validate_arg_count(uint64_t args_needed, uint64_t args_provided){
    if (args_provided != args_needed)
        return EXIT_FAILURE;
    return ARGS_OK;
}

