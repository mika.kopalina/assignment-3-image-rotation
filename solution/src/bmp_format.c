#include "bmp_format.h"
#include "image.h"
#include <stdio.h>

#define BM_HEADER 19778
#define BFBITCount 24
#define BM_RESERVED 0
#define B_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define X_PELS_PER_METER 2834
#define Y_PELS_PER_METER 2834
#define CLRS_USED 0
#define CLR_IMPORTANT 0
#define BYTES_OF_FOUR 4
#define ONE_PIXEL 3

// Функция для вычисления заполнения (padding)
static uint32_t get_padding(const uint32_t width) {
    return (BYTES_OF_FOUR - (width * ONE_PIXEL) % BYTES_OF_FOUR) % BYTES_OF_FOUR;
}

// Функция для чтения BMP файла и создания изображения
static enum read_status read_bmp_header(FILE *file, struct bmp_header *bmp) {
    if (!fread(bmp, sizeof(struct bmp_header), 1, file)) {
        return READ_INVALID_HEADER;
    }
    if (bmp->bfType != BM_HEADER) {
        return READ_INVALID_SIGNATURE;
    }
    if (bmp->biBitCount != BFBITCount) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

static enum read_status read_bmp_pixels(FILE *file, struct image *img, const struct bmp_header *bmp) {
    *img = image_makenew(bmp->biWidth, bmp->biHeight);
    if (img == NULL) {
        return SEGFAULT;
    }
    if (!img->data) {
        return OUT_OF_MEMORY;
    }
    uint32_t padding = get_padding(img->width);
    for (uint32_t i = 0; i < bmp->biHeight; i++) {
        if (fread(img->data + i * bmp->biWidth, sizeof(struct pixel), bmp->biWidth, file) != bmp->biWidth) {
            return READ_INVALID_PX_LINE;
        }
        if (fseek(file, (long)padding, SEEK_CUR) != 0) {
            return COULD_NOT_IGNORE_TRASH;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *file, struct image *img) {
    struct bmp_header bmp = {0};
    enum read_status status = read_bmp_header(file, &bmp);
    if (status != READ_OK) {
        return status;
    }
    return read_bmp_pixels(file, img, &bmp);
}

// Функция для создания BMP заголовка
static struct bmp_header init_header(const struct image* img) {
    uint32_t padding = get_padding(img->width);
    struct bmp_header new_header = {0};
    new_header.bfType = BM_HEADER;
    new_header.biSizeImage = (uint32_t) ((sizeof(struct pixel) * img->width + padding) * img->height);
    new_header.bfileSize = new_header.biSizeImage + sizeof(struct bmp_header);
    new_header.bfReserved = BM_RESERVED;
    new_header.bOffBits = sizeof(struct bmp_header);
    new_header.biSize = B_SIZE; // Размер структуры bmp_image_header
    new_header.biWidth = (uint32_t) img->width;
    new_header.biHeight = (uint32_t) img->height;
    new_header.biPlanes = PLANES;
    new_header.biBitCount = BFBITCount;  // Бит на пиксель
    new_header.biCompression = COMPRESSION;
    new_header.biXPelsPerMeter = X_PELS_PER_METER;
    new_header.biYPelsPerMeter = Y_PELS_PER_METER;
    new_header.biClrUsed = CLRS_USED;
    new_header.biClrImportant = CLR_IMPORTANT;
    return new_header;
}

static enum write_status write_bmp_header(FILE *file, const struct bmp_header *header) {
    size_t bmp_h = fwrite(header, sizeof(struct bmp_header), 1, file);
    if (bmp_h != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_bmp_pixels(FILE *file, const struct image *img) {
    uint32_t padding = get_padding(img->width);
    for (uint32_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * (img->width), sizeof(struct pixel), img->width, file) != img->width) {
            return WRITE_ERROR;
        }
        uint32_t zero[] = {0, 0, 0, 0};
        fwrite(zero, 1, padding, file);
    }
    return WRITE_OK;
}
// Функция для записи изображения в BMP файл
enum write_status to_bmp(FILE *file, const struct image *img) {
    struct bmp_header header = init_header(img);
    enum write_status status = write_bmp_header(file, &header);
    if (status != WRITE_OK) {
        return status;
    }
    return write_bmp_pixels(file, img);
}



