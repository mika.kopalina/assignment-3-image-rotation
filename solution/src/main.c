#include "file.h"
#include "printer.h"
#include "transformations.h"
#include "validation.h"
#include <stdlib.h>

#define ARGS_NEEDED 4
#define IMG_INPUT_INDEX 1
#define OUTPUT_INDEX 2
#define ANGLE_INDEX 3

// argv - это массив строк, который содержит аргументы командной строки, переданные в программу при её запуске
int main(int argc, char *argv[]) {
    uint64_t validated_arg_count = validate_arg_count(ARGS_NEEDED, (uint64_t)argc);
    if (validated_arg_count != 0) {
        fprintf(stderr,"Неверное количество аргументов. Формат должен быть: <исполняемый файл> <исходное изображение> <преобразованное изображение> <угол>");
        return EXIT_FAILURE;
    }

    const char* input_filename = argv[IMG_INPUT_INDEX];
    const char* output_filename = argv[OUTPUT_INDEX];
    int angle = atoi(argv[ANGLE_INDEX]) * (-1);


    // Проверка на допустимые значения угла
    if (angle != 0 && angle != 90 && angle != -90 && angle != 180 && angle != -180 && angle != 270 && angle != -270) {
        fprintf(stderr, "Недопустимый угол. Он должен быть целым значением из диапазона: -90, 90, -180, 180, -270, 270\n");
        return EXIT_FAILURE;
    }

    // Нормализация угла
    if (angle < 0) {
        angle = 360 + angle;
    }

    FILE* input_file = open_file_read(input_filename);
    if (input_file == NULL) {
        fprintf(stderr,"Файл нельзя открыть");
        return EXIT_FAILURE;
    }


    struct image img;
    enum read_status read_status = from_bmp(input_file, &img);
    fclose(input_file);
    if (read_status != READ_OK) {
        fprintf(stderr, "Ошибка чтения BMP файла: %d\n", read_status);
        return EXIT_FAILURE;
    }
    printf("Изображение успешно загружено\n");

    struct image rotated_img = rotate(&img, angle);
    image_delete(&img);
    if (rotated_img.data == NULL) {
        fprintf(stderr, "Ошибка при повороте изображения\n");
        return EXIT_FAILURE;
    }
    printf("Изображение изменено \n");

    FILE* output_file = open_file_write(output_filename);
    if (output_file == NULL) {
        fprintf(stderr,"Ошибка при открытии выходного файла");
        image_delete(&rotated_img);
        return EXIT_FAILURE;
    }

    enum write_status write_status = to_bmp(output_file, &rotated_img);
    fclose(output_file);
    image_delete(&rotated_img);

    if (write_status != WRITE_OK) {
        fprintf(stderr, "Ошибка записи BMP файла: %d\n", write_status);
        return EXIT_FAILURE;
    }

    printf("Изображение успешно обработано и сохранено в %s\n", output_filename);
    return 0;
}
