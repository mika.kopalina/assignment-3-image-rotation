#include "transformations.h"

struct image rotate(const struct image* img, int angle) {
    struct image rotated_img;
    // Проверка угла и инициализация изображения
    int angle_num = ((angle + 360) / 90) % 4;
    switch (angle_num) {
        case 1:
        case 3:
            rotated_img = image_makenew(img->height, img->width);
            break;
        case 0:
        case 2:
            rotated_img = image_makenew(img->width, img->height);
            break;
        default:
            // Для неподдерживаемых углов возвращаем пустое изображение
            return (struct image){0, 0, 0};
    }

    if (!rotated_img.data) {
        return (struct image){0, 0, 0};
    }

    // Осуществляем поворот
    for (uint64_t y = 0; y < img->height; ++y) {
        for (uint64_t x = 0; x < img->width; ++x) {
            struct pixel current_pixel = img->data[y * img->width + x];
            int angle_num = ((angle + 360) / 90) % 4;
            switch (angle_num) {
                case 0:
                    rotated_img.data[y * rotated_img.width + x] = current_pixel;
                    break;
                case 1:
                    rotated_img.data[x * rotated_img.width + (rotated_img.width - y - 1)] = current_pixel;
                    break;
                case 2:
                    rotated_img.data[(rotated_img.height - y - 1) * rotated_img.width + (rotated_img.width - x - 1)] = current_pixel;
                    break;
                case 3:
                    rotated_img.data[(rotated_img.height - x - 1) * rotated_img.width + y] = current_pixel;
                    break;
            }
        }
    }
    return rotated_img;
}
