#ifndef IMAGE_TRANSFORMER_VALIDATION_H
#define IMAGE_TRANSFORMER_VALIDATION_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define ARGS_OK 0
#define ACCEPTABLE_ANGLES \
    X(ANGLE_90, 90)       \
    X(ANGLE_NEG_90, -90) \
    X(ANGLE_180, 180)     \
    X(ANGLE_NEG_180, -180) \
    X(ANGLE_270, 270)     \
    X(ANGLE_NEG_270, -270)

#define X(name, value) name = (value),
enum {
    ACCEPTABLE_ANGLES
};
#undef X

uint64_t validate_arg_count(uint64_t args_needed, uint64_t args_provided);
#endif //IMAGE_TRANSFORMER_VALIDATION_H

