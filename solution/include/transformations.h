#ifndef IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#define IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#include "image.h"
struct image rotate(const struct image* img, int angle);

#endif //IMAGE_TRANSFORMER_TRANSFORMATIONS_H

