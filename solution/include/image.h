#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include <stdint.h>

// Структура пикселя
struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct pixel pixel_new(int8_t r, int8_t g, int8_t b);

// Структура изображения
struct image {
    uint64_t width, height;
    struct pixel *data;
};

// Создание изображения с заданными размерами
struct image image_makenew(const uint64_t width, const uint64_t height);

// Освобождение памяти, занятой изображением
void image_delete(struct image* img);

#endif // IMAGE_TRANSFORMER_IMAGE_H


