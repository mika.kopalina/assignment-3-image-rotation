#ifndef IMAGE_TRANSFORMER_PRINTER_H
#define IMAGE_TRANSFORMER_PRINTER_H
#include <stdio.h>
#define DEFINE_PRINT(stream) \
        void print_##stream(const char* message){ \
            fprintf(stream, "%s\n", message);                        \
        }
DEFINE_PRINT(stderr)
DEFINE_PRINT(stdout)
#endif //IMAGE_TRANSFORMER_PRINTER_H

