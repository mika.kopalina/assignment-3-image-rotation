#ifndef IMAGE_TRANSFORMER_BMP_FORMAT_H
#define IMAGE_TRANSFORMER_BMP_FORMAT_H

#include "image.h"
#include <stdio.h>

// Структура для BMP файлового заголовка
struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    SEGFAULT,
    OUT_OF_MEMORY,
    READ_INVALID_BITS,
    READ_INVALID_PX_LINE,
    COULD_NOT_IGNORE_TRASH
};

enum read_status from_bmp(FILE* file, struct image* img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* file, struct image const *img);

#endif //IMAGE_TRANSFORMER_BMP_FORMAT_H

